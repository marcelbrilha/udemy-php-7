<?php

spl_autoload_register(function ($className) {

    $dirClass = "class";
    $fullPath = $dirClass . DIRECTORY_SEPARATOR . "$className.php";

    if (file_exists($fullPath)) {

        require_once($fullPath);
    }
});
