<?php

function tratarNome($name) {
    if (!$name) {
        throw new Exception("Nenhum nome foi informado.");
    }

    echo ucfirst($name) . "<br />";
}

try {
    tratarNome("joão");
    tratarNome("");
} catch (Exception $e) {
    echo $e->getMessage() . "<br />";
} finally {
    echo "Executou o bloco try";
}
