<?php

class Usuario {

    private $idusuario;
    private $deslogin;
    private $dessenha;
    private $dtcadastro;

    public function __construct($login = "", $senha = "") {
        $this->deslogin = $login;
        $this->dessenha = $senha;
    }

    public function getId() {
        return $this->idusuario;
    }

    public function setId($idusuario) {
        $this->idusuario = $idusuario;
    }

    public function getDeslogin() {
        return $this->deslogin;
    }

    public function setDeslogin($deslogin) {
        $this->deslogin = $deslogin;
    }

    public function getDessenha() {
        return $this->dessenha;
    }

    public function setDessenha($dessenha) {
        $this->dessenha = $dessenha;
    }

    public function getDtCadastro() {
        return $this->dtcadastro;
    }

    public function setDtCadastro($dtcadastro) {
        $this->dtcadastro = $dtcadastro;
    }

    public function __toString() {
        return json_encode(array(
            "idusuario" => $this->idusuario,
            "deslogin" => $this->deslogin,
            "dessenha" => $this->dessenha,
            "dtcadastro" => $this->dtcadastro->format("d/m/Y H:i:s")
        ));
    }

    public function loadById($id) {
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM tb_usuarios WHERE idusuario = :ID", array(
            ":ID" => $id
        ));

        if (count($results[0]) > 0) {
            $this->setData(results[0]);
        }
    }

    public static function getList() {
        $sql = new Sql();
        return $sql->select("SELECT * FROM tb_usuarios ORDER BY deslogin");
    }

    public static function getSearch($login) {
        $sql = new Sql();
        return $sql->select("SELECT * FROM tb_usuarios WHERE deslogin LIKE :SEARCH", array(
            ':SEARCH' => "%".$login."%"
        ));
    }

    public function login($login, $password) {
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM tb_usuarios WHERE deslogin = :LOGIN AND dessenha = :SENHA", array(
            ":LOGIN" => $login,
            ":SENHA" => $password
        ));

        if (count($results[0]) > 0) {
            $this->setData(results[0]);
        } else {
            throw new Exception("Login ou senha inválidos");
        }
    }

    public function setData($data) {
        $this->setId($data['idusuario']);
        $this->setDeslogin($data['deslogin']);
        $this->setDessenha($data['dessenha']);
        $this->setDtCadastro(new DateTime($data['dtcadastro']));
    }

    public function insert() {
        $sql = new Sql();
        $results = $sql->select("CALL sp_usuarios_insert(:LOGIN, :PASSWORD)", array(
           ':LOGIN' => $this->deslogin,
           ':PASSWORD' => $this->dessenha
        ));

        if (count($results[0]) > 0) {
            $this->setData(results[0]);
        } else {
            throw new Exception("Login ou senha inválidos");
        }
    }

    public function update($login, $password) {
        $this->setDeslogin($login);
        $this->setDessenha($password);

        $sql = new Sql();
        $results = $sql->query("UPDATE tb_usuarios SET deslogin = :LOGIN, dessenha = :PASSWORD WHERE idusuario = :ID", array(
           ':LOGIN' => $this->deslogin,
           ':PASSWORD' => $this->dessenha,
           ':ID' => $this->idusuario
        ));
    }

    public function delete() {
        $sql = new Sql();
        $sql->query("DELETE FROM tb_usuarios WHERE idusuario = :ID", array(
            ':ID' => $this->idusuario
        ));

        $this->setId(0);
        $this->setDeslogin("");
        $this->setDessenha("");
        $this->setDtCadastro(new DateTime());
    }
}
