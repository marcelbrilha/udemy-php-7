<?php

// include "exemplo-01.php"; // Insere o arquivo e caso ocorra um erro ele continua a execução
// require "exemplo-01.php"; // O arquivo precisa existir e estar funcional - senão fatalError
require_once "exemplo-01.php"; // Insere o arquivo somente uma vez

$resultado = somar(10, 20);

echo $resultado;
