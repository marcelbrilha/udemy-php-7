<?php 
	
	// Tipos Básicos
	$nome = "HCode";
	$site = 'www.hcode.com.br';

	$ano = 1990;
	$salario = 5500.99;
	$bloqueado = false;

	// Tipos compostos
	$frutas = array("abacaxi", "laranja", "manga");

	echo $frutas[0];

	$dataAtual = new DateTime();

	echo "<br />";

	var_dump($dataAtual);

	// Tipos especiais
	$arquivo = fopen("exemplo-03.php", "r");

	echo "<br />";

	var_dump($arquivo);

	$nulo = null;
	$vazio = "";

