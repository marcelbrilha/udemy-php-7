<?php

$dirName = "images";

if (!is_dir($dirName)) {
    mkdir($dirName); // Criar diretório
} else {
    rmdir($dirName); // Remover diretório
}
