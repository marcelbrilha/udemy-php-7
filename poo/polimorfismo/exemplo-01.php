<?php

abstract class Animal {
    public function falar() {
        return "Som";
    }

    public function mover() {
        return "Andar";
    }
}

class Cachorro extends Animal {

    public function falar() {
        return "Latir";
    }
}

class Gato extends Animal {

    public function falar() {
        return "Miar";
    }
}

class Passaro extends Animal {

    public function falar() {
        return "Cantar";
    }

    public function mover() {
        return "Voar e " . parent::mover();
    }
}

$pluto = new Cachorro();
echo $pluto->falar() . "<br>";
echo $pluto->mover() . "<br>";;

$garfield = new Gato();
echo $garfield->falar() . "<br>";
echo $pluto->mover() . "<br>";

$passarinho = new Passaro();
echo $passarinho->falar() . "<br>";
echo $passarinho->mover();
