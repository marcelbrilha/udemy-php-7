<?php

class Pessoa {

    public $nome;

    public function falar() {
        return "O meu nome é: " . $this->nome;
    }
}

$marcel = new Pessoa();

$marcel->nome = "Marcel";

echo $marcel->falar();
