<?php

class Documento {

    private $numero;

    public function getNumero(): int {
        return $this->numero;
    }

    public function setNumero(int $numero) {
        return $this->numero = $numero;
    }
}

class CPF extends Documento {

    public function validar(): bool {
        // TODO: Construir validação de CPF
        return true;
    }
}

$doc = new CPF();
$doc->setNumero('765053918-44');
var_dump($doc->validar());
