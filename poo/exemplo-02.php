<?php

class Carro {

    private $modelo;

    public function getModelo(): string {
        return $this->modelo;
    }

    public function setModelo($modelo) {
        $this->modelo = $modelo;
    }
}

$gol = new Carro();
$gol->setModelo("GT");
var_dump($gol->getModelo());
