<?php

class Endereco {

    private $logradouro;
    private $numero;
    private $cidade;

    public function __construct($logradouro, $numero, $cidade) {
        $this->logradouro = $logradouro;
        $this->numero = $numero;
        $this->cidade = $cidade;
    }

    public function __destruct() {
        var_dump("Destruir");
    }

    public function getLogradouro(): string {
        return $this->logradouro;
    }

    public function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
    }

    public function getNumero(): int {
        return $this->numero;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function getCidade(): string {
        return $this->cidade;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    public function __toString(): string {
        return $this->logradouro . ", " . $this->numero . ", " . $this->cidade;
    }
}


$endereco = new Endereco("Rua hermelinda de oliveira", 85, "Vila Carpi");

echo $endereco->getLogradouro();

echo "<br />";

echo $endereco->getNumero();

echo "<br />";

echo $endereco->getCidade();

echo "<br />";

echo $endereco;

echo "<br />";

unset($endereco);