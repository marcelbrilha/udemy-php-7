<?php

class Documento {

    private $numero;

    public function getNumero(): int {
        return $this->numero;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public static function validarCPF(): bool {
        return false;
    }
}

$cpf = new Documento();
$cpf->setNumero("123456789");

var_dump(Documento::validarCPF());

echo "<br>";

var_dump($cpf->getNumero());
